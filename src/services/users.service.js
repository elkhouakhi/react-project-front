const baseUrl = "http://localhost:3002";

class VoitureService{

    static async list(){
        let init = {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        };
        let call = await fetch(`${baseUrl}/users`, init);
        return call;
    }

    
     static async create(body){
        let init = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(body)
        };
        let call = await fetch(`${baseUrl}/users`, init);
        return call;
    }



}

export default VoitureService;