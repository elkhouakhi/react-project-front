import React, {Component} from 'react';
import VoitureUpdate from '../components/VoitureUpdate';
import VoitureService from '../services/voitures.service';
import SearchVoitures from '../components/SearchVoitures';
import {Link} from 'react-router-dom';



class Gestion extends Component{

    constructor(props){
        super(props);

        this.state = {
            voitures: []
        };


    }

    async componentDidMount(){    
         let response = await VoitureService.list();
         if(response.ok){
             let data = await response.json();
             this.setState({voitures: data.voitures});     
         }
     }

     async deletePost(id){
        let response = await VoitureService.delete(id);
        if(response.ok){
            let voitures = this.state.voitures;
            let index = voitures.findIndex(voiture => voiture.id === id);
            voitures.splice(index, 1);

            this.setState({voitures: voitures});
        }
    }


    render(){
        return (
            <div className="container">
                    <a className="btn btn-success mt-3 mb-3"> <Link className="nav-link" to={`/addVoiture`}>Ajouter</Link>
                    </a>
                
                 
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Model</th>
                            <th>Prix</th>
                            <th>photo</th>                 
                            <th><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        {               
                                this.state.voitures.map((item, index) => {
                                    return (
                                        <VoitureUpdate key={index} data={item} deletePost={(id) => this.deletePost(id)}/>
                                    )
                                })
                   
                        }
                    </tbody>
                </table>
            </div>
        )
    }

}

export default Gestion;