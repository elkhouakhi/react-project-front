import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import VoitureService from '../services/voitures.service';




class UpdCar extends Component{

   
    constructor(props){
        super(props);
        this.state = {
            voitures : [],
            model: '',
            prix: '',
            photo: '',
            success: false,
            msgSuccess: '',
        }  
    }



    

 async componentDidMount(){    
    const id =  this.props.match.params.id;
    let response = await VoitureService.details(id);
    if(response.ok){
        let data = await response.json();
        this.setState({voitures: data.voiture});

    }
}


handleChange(e){
    //change state
    this.setState({
        [e.target.id]: e.target.value
    });
}


async submit(e){
    e.preventDefault();
    this.setState({success: false});
    let body = {
        model: this.state.model,
        prix: this.state.prix,
        photo: this.state.photo,
    }; 
    const id =  this.props.match.params.id;
    let response = await VoitureService.update(id, body);
    if(response.ok){
        this.setState({
            success: true,
            msgSuccess: "Voiture Modifiée"
        })
    }
}





    render(){
        return (
        
            <div className="row">        

            <div className="container">
                <h1>Modifier la voiture {this.state.voitures.model} </h1>
                <form >
             <div className="col-md-6">
             <div className="form-group">
                        <label>Model</label>
        <input type="text" value={this.state.voitures.model} id="model" onChange={(e) => this.handleChange(e)} className="form-control"  id="title" />
             </div>

             </div>
                 
            <div className="col-md-6">
            <div className="form-group">
                        <label>Prix</label>
                        <input type="text" value={this.state.voitures.prix} id="prix" onChange={(e) => this.handleChange(e)} className="form-control"  id="content" />
                    </div>
           

            </div> 

            <div className="col-md-6">
             <div className="form-group">
                        <label>photo</label>
                        <input type="text" value={this.state.voitures.photo} id="photo"  onChange={(e) => this.handleChange(e)} className="form-control"  id="title" />
             </div>
             <center> <button type="submit"  className="btn btn-primary"> Modifier </button>  </center>   
             </div>
                </form>
                {
                    this.state.success === true ? <p>{this.state.msgSuccess}</p> : null 
                }

           
            </div>
            </div>
        )
    }

}

export default UpdCar;