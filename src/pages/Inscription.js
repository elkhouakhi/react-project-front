import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import UserService from '../services/users.service';




class Inscription extends Component{

 
    constructor(props){
        super(props);
        this.state = {
            nom: '',
            prenom: '',
            email: '',
            password: '',
            success: false,
            msgSuccess: '',
        }  
    }

    //Change value instead of id state
    handleChange(e){
        //change state
        this.setState({
            [e.target.id]: e.target.value
        });
    }
    
    async submit(e){
        e.preventDefault();
        this.setState({success: false});
        let body = {
            nom: this.state.nom,
            prenom: this.state.prenom,
            email: this.state.email,
            password: this.state.password
            }; 
        let response = await UserService.create(body);
        if(response.ok){
            this.setState({
                success: true,
                msgSuccess: "voiture ajoutées avec succès"
            })
        }
    }

    render(){
        return (
        
            <div className="row">        

            <div className="container">
                <h1>Inscrivez-Vous !!!</h1>
                <form  onSubmit={(e) => this.submit(e)}>
             <div className="col-md-6">
             <div className="form-group">
                        <label>Nom</label>
                        <input type="text" className="form-control" placeholder="Votre nom" required id="nom" onChange={(e) => this.handleChange(e)}/>
             </div>

             </div>
                 
            <div className="col-md-6">
            <div className="form-group">
                        <label>Prenom</label>
                        <input type="text" className="form-control" placeholder="Votre prenom" required id="prenom"  onChange={(e) => this.handleChange(e)} />
                    </div>
           

            </div> 

            <div className="col-md-6">
            <div className="form-group">
                        <label>Email</label>
                        <input type="text" className="form-control" placeholder="Email" required id="email"  onChange={(e) => this.handleChange(e)} />
                    </div>
           

            </div> 

            <div className="col-md-6">
             <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" placeholder="Votre mot de passe" required  id="password" onChange={(e) => this.handleChange(e)}/>
             </div>
             <center> <button type="submit" className="btn btn-primary"> Inscription </button>  </center>   
             </div>
                </form>
                {
                    this.state.success === true ? <p>{this.state.msgSuccess}</p> : null 
                }
            </div>
            </div>
        )
    }

}

export default Inscription;