import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import VoitureService from '../services/voitures.service';




class AddCar extends Component{

 
    constructor(props){
        super(props);
        this.state = {
            model: '',
            prix: '',
            photo: '',
            success: false,
            msgSuccess: '',
        }  
    }

    //Change value instead of id state
    handleChange(e){
        //change state
        this.setState({
            [e.target.id]: e.target.value
        });
    }
    
    async submit(e){
        e.preventDefault();
        this.setState({success: false});
        let body = {
            model: this.state.model,
            prix: this.state.prix,
            photo: this.state.photo,
            }; 
        let response = await VoitureService.create(body);
        if(response.ok){
            this.setState({
                success: true,
                msgSuccess: "voiture ajoutées avec succès"
            })
        }
    }

    render(){
        return (
        
            <div className="row">        

            <div className="container">
                <h1>Ajouter Une voiture</h1>
                <form  onSubmit={(e) => this.submit(e)}>
             <div className="col-md-6">
             <div className="form-group">
                        <label>Model</label>
                        <input type="text" className="form-control" required id="model" value={this.state.model} onChange={(e) => this.handleChange(e)}/>
             </div>

             </div>
                 
            <div className="col-md-6">
            <div className="form-group">
                        <label>Prix</label>
                        <input type="text" className="form-control" required id="prix" value={this.state.prix} onChange={(e) => this.handleChange(e)} />
                    </div>
           

            </div> 

            <div className="col-md-6">
             <div className="form-group">
                        <label>photo</label>
                        <input type="text" className="form-control" required  id="photo" value={this.state.photo} onChange={(e) => this.handleChange(e)}/>
             </div>
             <center> <button type="submit" className="btn btn-primary"> Ajouter </button>  </center>   
             </div>
                </form>
                {
                    this.state.success === true ? <p>{this.state.msgSuccess}</p> : null 
                }
            </div>
            </div>
        )
    }

}

export default AddCar;