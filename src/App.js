import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from './pages/Home';
import Header from './components/Header';
import Footer from './components/Footer';
import VoitureItem from './components/VoitureItem';
import Login from './pages/Login';
import Gestion from './pages/Gestion';
import AddCar from './pages/AddCar';
import UpdCar from './pages/UpdCar';
import Inscription from './pages/Inscription';


class App extends Component{

    render(){
        return (       
            <BrowserRouter>
          
            <Header />
                <Route path="/" exact component={Home} />
                <Route path="/voitures" exact component={Home} />
                <Route path="/voitures/:id" exact component={VoitureItem} />
                <Route path="/LOGIN" exact component={Login} />
                <Route path="/Gerer" exact component={Gestion} />
                <Route path="/addVoiture" exact component={AddCar} />
                <Route path="/ModifierVoiture/:id" exact component={UpdCar} />
                <Route path="/inscription" exact component={Inscription} />

               
                <Switch>
                <Footer />
       
            </Switch>
           
          
            </BrowserRouter>
        )
    }
}

export default App;
