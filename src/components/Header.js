import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component{

    constructor(props){
        super(props);
    }

    render(){      return(
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                 <a className="navbar-brand" href="#"><Link className="nav-link" to={'/'}>Location Car</Link></a>
            <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="collapsibleNavId">
                <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">
                        <Link className="nav-link" to={'/'}>Accueil</Link>

                         <span class="sr-only">(current)</span></a>
                    </li>
   
                    <li className="nav-item mr-200">
                        <a className="nav-link" href="#">
                        <li class="nav-item">
                                <Link className="nav-link" to={'/lOGIN'}>LOGIN</Link>
                            </li>
                        </a>
                    </li>
                    
                </ul>
              
            </div>
        </nav>
      )
    }


}


export default Header;