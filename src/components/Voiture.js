import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Voiture extends Component{

    constructor(props){
        super(props);
    
    }

    componentDidMount(){
       
    }



    render() {
        return (
           
         <div className="row">
           
               <div className="card">
               <img className="card-img-top" src={require(`./../images/${this.props.data.photo}`)}/>  
   
                        <div className="card-body">
                             <h4 className="card-title">{this.props.data.model}</h4>
                            <p className="card-text">{this.props.data.prix}</p>
                            <a  className="btn btn-success"> <Link to={`/voitures/${this.props.data._id}`}>Plus d'infos</Link></a>
                    

                        </div>
                    </div>
                    </div>
               
           
        )
    }

    


}

export default Voiture;