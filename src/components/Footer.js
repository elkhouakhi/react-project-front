import React, {Component} from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";

class Footer extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return (  <MDBFooter color="blue" className="font-small pt-4 mt-4">
        <MDBContainer fluid className="text-center text-md-left">
          <MDBRow>
            <MDBCol md="6">
              <h5 className="title">Location Car</h5>
              <p>
               Espace d'exposition des voitures proposés en location
              </p>
            </MDBCol>
            <MDBCol md="6">
              <h5 className="title">Nos Agences</h5>
              <ul>
                <li className="list-unstyled">
                  <a href="#!">Paris 75008</a>
                </li>
                <li className="list-unstyled">
                  <a href="#!">Paris 75009</a>
                </li>
                <li className="list-unstyled">
                  <a href="#!">Paris 75011</a>
                </li>
                <li className="list-unstyled">
                  <a href="#!">Paris 75015</a>
                </li>
              </ul>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
        <div className="footer-copyright text-center py-3">
          <MDBContainer fluid>
            &copy; {new Date().getFullYear()} Copyright: <a > LocationCar.fr </a>
          </MDBContainer>
        </div>
      </MDBFooter>
        )
    }

}

export default Footer;