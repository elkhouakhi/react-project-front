import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Voiture from './Voiture';
import axios from 'axios';

class VoitureItem extends Component{
    constructor(props){
        super(props);
    
    }
   state = {
            car: []
        };

    componentWillMount() {
       const id =  this.props.match.params.id;
       axios.get(`http://localhost:3002/voitures/${id}`)
       .then(res => {
           this.setState({
            car: res.data.voiture,
            carPhoto: res.data.voiture.photo
         })
         console.log(res.data.voiture)      
    })
}


    render(){
        return(
    <div className="container mt-10">
      <div className="row mt-10">
             <div className="card mt-20 ">
                    {/*<img className="card-img-top" src={require(`./images/${this.state.car.photo}`)}/> */} 
                            <div className="card-body mt-15">                               
                                <h4 className="card-title">{this.state.car.model}</h4>
                                <p className="card-text">{this.state.car.prix}</p>
                                <p>Pour réserver appeler nous sur les numéros affichés ci-dessous!!</p>
                                <p>Location Car est à votre service</p>
                            </div>
                    </div>
        </div>
    </div> 
         
              
      
        )
    }
    



}

export default VoitureItem;